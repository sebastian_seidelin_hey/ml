# To make TF shut up ...
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.get_logger().setLevel('ERROR')

import tensorflow
from tensorflow.keras.datasets import cifar10
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv2D, MaxPooling2D, Flatten, BatchNormalization
from tensorflow.keras.optimizers import RMSprop
from keras.preprocessing.image import ImageDataGenerator

(train_images, train_labels), (test_images, test_labels) = cifar10.load_data()
from tensorflow.keras import backend as K

if K.image_data_format() == 'channels_first':
    train_images = train_images.reshape(train_images.shape[0], 3, 32, 32)
    test_images = test_images.reshape(test_images.shape[0], 3, 32, 32)
    input_shape = (3, 32, 32)
else:
    train_images = train_images.reshape(train_images.shape[0], 32, 32, 3)
    test_images = test_images.reshape(test_images.shape[0], 32, 32, 3)
    input_shape = (32, 32, 3)
    
train_images = train_images.astype('float32')
test_images = test_images.astype('float32')
train_images /= 255
test_images /= 255

train_labels = tensorflow.keras.utils.to_categorical(train_labels, 10)
test_labels = tensorflow.keras.utils.to_categorical(test_labels, 10)

def build_model(dense_dropout=0.2, dense_num_nodes=256, dense_layers=6):
    model = Sequential()

    model.add(Conv2D(32, kernel_size=(3, 3), activation='relu', kernel_initializer='he_uniform', padding='same', input_shape=input_shape))
    model.add(BatchNormalization())
    model.add(Conv2D(32, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(64, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(BatchNormalization())
    model.add(Conv2D(128, (3, 3), activation='relu', kernel_initializer='he_uniform', padding='same'))
    model.add(BatchNormalization())
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.2))

    model.add(Flatten())


    for _ in range(dense_layers):
        model.add(Dense(dense_num_nodes, activation='relu', kernel_initializer='he_uniform'))
        model.add(BatchNormalization())
        if dense_dropout > 0:
            model.add(Dropout(dense_dropout))

    model.add(Dense(10, activation='softmax'))

    model.summary()

    return model

def evaluate_model(model, epochs=30, batch_size=64, verbose=2):
    model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

    datagen = ImageDataGenerator(width_shift_range=0.1, height_shift_range=0.1, horizontal_flip=True)
    train_iterator = datagen.flow(train_images, train_labels, batch_size=batch_size)

    history = model.fit(train_iterator,
                        batch_size=batch_size,
                        epochs=epochs,
                        verbose=verbose,
                        validation_data=(test_images, test_labels))

    score = model.evaluate(test_images, test_labels, verbose=0)
    if verbose == 2:
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])

    return history

model = build_model(dense_layers=1, dense_num_nodes=128, dense_dropout=0.2)
evaluate_model(model, epochs=100)