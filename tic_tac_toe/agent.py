"""An agent and human class used to play a Tic-Tac-Toe game."""
from environment import Piece
import numpy as np


class Agent(object):
    """docstring for Agent."""

    def __init__(self, symbol=None, epsilon=0.1, alpha=0.5):
        """Initialize the Agent."""
        # Use the Pieces class from the environment!
        self.symbol = symbol
        self.epsilon = epsilon
        self.alpha = alpha
        self.verbose = False
        self.state_history = []

    def set_value(self, value):
        """Set the value list for the Agent."""
        self.value = value

    def set_symbol(self, symbol):
        """Set the symbol for the Agent."""
        self.symbol = symbol

    def set_verbose(self, verbose):
        """Set if Agent should print info about its moves."""
        self.verbose = verbose

    def reset_history(self):
        """Reset the state history of the Agent."""
        self.state_history = []

    def take_action(self, environment):
        """Make Agent take an action in the game."""
        if self.symbol is None:
            raise Exception("Type not set, can't place piece on board")

        r = np.random.rand()
        # best_state = None
        if r < self.epsilon:
            # Random action
            if self.verbose:
                print ("Taking random action")
            possible_moves = environment.get_empty_slots()
            idx = np.random.choice(len(possible_moves))
            next_move = possible_moves[idx]
        else:
            # Take a more clever action
            pos2value = {}
            next_move = None
            best_value = -1
            for i in range(environment.state.shape[0]):
                for j in range(environment.state.shape[1]):
                    if environment.is_empty(i, j):
                        environment.state[i, j] = self.symbol
                        state = environment.encode_board()
                        environment.state[i, j] = Piece.Empty
                        pos2value[(i, j)] = self.value[state]
                        if self.value[state] > best_value:
                            best_value = self.value[state]
                            # best_state = state
                            next_move = (i, j)

            if self.verbose:
                print ("Taking a greedy action")
                for i in range(environment.state.shape[0]):
                    print ('----------------')
                    for j in range(environment.state.shape[1]):
                        if environment.is_empty(i, j):
                            print ("%.2f|" % pos2value[i, j],)
                        else:
                            print (' ',)
                            if environment.check_board(i, j) == Piece.X:
                                print ('x |',)
                            elif environment.check_board(i, j) == Piece.O:
                                print ('o |',)
                            else:
                                print ('  |',)
                    print ('')
                print ('----------------')
        environment.update_board(next_move[0], next_move[1], self.symbol)

    def update(self, environment):
        """Update the Agent's reward table."""
        reward = environment.reward(self.symbol)
        target = reward
        for prev in reversed(self.state_history):
            value = self.value[prev] + self.alpha * (target - self.value[prev])
            self.value[prev] = value
            target = value
        self.reset_history()

    def update_state_history(self, state):
        """Update the Agent's state history."""
        self.state_history.append(state)

    # Used to pretty print the board
    def __str__(self):
        """Print the symbol of the Agent."""
        return str(self.symbol)

    # Used to compare during a game
    def __eq__(self, other):
        """Compare this Agent with another Agent."""
        if type(self) == type(other):
            return self.symbol == other.symbol
        else:
            return False


class Human(object):
    """docstring for Human."""

    def __init__(self):
        """Initialize the human class."""
        pass

    def set_symbol(self, symbol):
        """Set the symbol of the player."""
        self.symbol = symbol

    def take_action(self, environment):
        """Make the human take an action."""
        while True:
            move = input("Enter coordinates i,j for next move: ")
            print (move)
            i, j = move.split(',')
            i = int(i)
            j = int(j)
            if environment.update_board(i, j, self.symbol):
                break

    def update(self, environment):
        """Update the player."""
        pass

    def update_state_history(self, state):
        """Update the state history."""
        pass
