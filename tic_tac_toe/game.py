"""Play a game of tic-tac-toe against the computer."""
import agent
import numpy as np
from environment import Piece, Environment


def play_game(player_one, player_two, environment, draw=False):
    """Play a game of tic-tac-toe."""
    player = None

    while not environment.game_over():
        if player == player_one:
            player = player_two
        else:
            player = player_one

        if draw:
            environment.draw_board()

        player.take_action(environment)
        player_one.update_state_history(environment.encode_board())
        player_two.update_state_history(environment.encode_board())

    if draw:
        if draw is 1 and player is player_one:
            environment.draw_board()
        if draw is 2 and player is player_two:
            environment.draw_board()

    player_one.update(environment)
    player_two.update(environment)


def get_state_hash_and_winner(environment, i=0, j=0):
    """Iterate all states and get rewards, used for initialization of agent."""
    results = []

    for v in Piece.Empty, Piece.X, Piece.O:
        environment.state[i, j] = v
        if j is 2:
            if i is 2:
                # Board is full
                state = environment.encode_board()
                ended = environment.game_over(force_recalculate=True)
                winner = environment.winner
                results.append((state, winner, ended))
            else:
                results += get_state_hash_and_winner(environment, i + 1, 0)
        else:
            results += get_state_hash_and_winner(environment, i, j+1)

    return results


def initial_value_x(environment, state_winner_triples):
    """Set the initial values for the agent using the 'X' symbol."""
    V = np.zeros(environment.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == Piece.X:
                v = 1
            else:
                v = 0
        else:
            v = 0.5
        V[state] = v
    return V


def initial_value_o(environment, state_winner_triples):
    """Set the initial values for the agent using the 'O' symbol."""
    V = np.zeros(environment.num_states)
    for state, winner, ended in state_winner_triples:
        if ended:
            if winner == Piece.X:
                v = 0
            else:
                v = 1
        else:
            v = 0.5
        V[state] = v
    return V


if __name__ == '__main__':
    # Set up and prep agents for playing
    agent_x = agent.Agent(Piece.X)
    agent_o = agent.Agent(Piece.O)
    training_env = Environment()
    initial_values = get_state_hash_and_winner(training_env)
    agent_x.set_value(initial_value_x(training_env, initial_values))
    agent_o.set_value(initial_value_o(training_env, initial_values))

    T = 10000
    for t in range(T):
        if t % 200 == 0:
            print (t)
        play_game(agent_x, agent_o, Environment())

    human = agent.Human()
    human.set_symbol(Piece.O)
    while True:
        agent_x.set_verbose(True)
        play_game(agent_x, human, Environment(), 2)
        answer = input('Play again? [Y/n]: ')
        if answer and answer.lower()[0] is 'n':
            break
