"""An evnironment to play a game of tic-tac-toe in."""
import numpy as np

LENGTH = 3


class Piece:
    """Represents a piece to be placed on the board."""

    Empty = 0
    X = 1
    O = 2


class Environment(object):
    """docstring for Environment."""

    def __init__(self):
        """Initialize the environment."""
        # Setup an empty 3x3 board
        self.reset_state()
        self.last_placed_piece = Piece.Empty
        self.winner = None
        self.ended = False
        self.num_states = 3**(LENGTH * LENGTH)

    def check_board(self, i, j):
        """Get the piece situated a (i,j)."""
        return self.state[i, j]

    def is_empty(self, i, j):
        """Test if board is empty at position (i,j)."""
        return self.state[i, j] == Piece.Empty

    def reward(self, symbol):
        """Calculate the reward, if any, for a given symbol."""
        if not self.game_over():
            return 0

        return 1 if symbol == self.winner else 0

    def update_board(self, i, j, piece):
        """Place a piece on the board."""
        # Check if input is valid
        if i * j > self.state.size:
            return False

        # Only update the board if place
        if self.check_board(i, j) == Piece.Empty:
            self.state[i, j] = piece
            self.last_placed_piece = piece
            return True
        else:
            return False

    def game_over(self, force_recalculate=False):
        """Tell whether game is over or not."""
        if not force_recalculate and self.ended:
            return self.ended

        for i in range(0, 3):
            row = self.state[i]
            col = self.state[:, i]

            if row[0] == row[1] == row[2] != Piece.Empty:
                self.winner = row[0]
                self.ended = True
                return True

            if col[0] == col[1] == col[2] != Piece.Empty:
                self.winner = col[0]
                self.ended = True
                return True

        # Upper left to lower right diagonal
        board = self.state
        if board[0, 0] == board[1, 1] == board[2, 2] != Piece.Empty:
            self.winner = board[0, 0]
            self.ended = True
            return True

        # Upper right to lower left diagonal
        if board[2, 0] == board[1, 1] == board[0, 2] != Piece.Empty:
            self.winner = board[2, 0]
            self.ended = True
            return True

        # We're out of positions to fill and no winner, it's a draw
        if not (Piece.Empty in board.flat):
            self.winner = None
            self.ended = True
            return True

        # Default case, not game over and no winner
        return False

    def encode_board(self):
        """Encode the board as a base 3 integer."""
        k = 0
        h = 0
        for i in range(LENGTH):
            for j in range(LENGTH):
                if self.state[i, j] == Piece.Empty:
                    v = 0
                elif self.state[i, j] == Piece.X:
                    v = 1
                elif self.state[i, j] == Piece.O:
                    v = 2
                h += (3**k) * v
                k += 1
        return h

    def get_empty_slots(self):
        """Get the empty slots of the current board."""
        empty_slots = []
        for i in range(self.state.shape[0]):
            for j in range(self.state.shape[1]):
                if self.check_board(i, j) == Piece.Empty:
                    empty_slots.append((i, j))
        return empty_slots

    def get_state(self):
        """Get the current board."""
        return self.state

    def drawable_piece(self, piece):
        """Get a drawable representation of the piece."""
        if piece == Piece.Empty:
            return '-'
        elif piece == Piece.X:
            return 'X'
        else:
            return 'O'

    def draw_board(self):
        """Draw the current board in a pretty way."""
        print ('-------------')
        print ('| ' + self.drawable_piece(self.state[0, 0]) + ' | ' + \
            self.drawable_piece(self.state[0, 1]) + ' | ' + \
            self.drawable_piece(self.state[0, 2]) + ' |')
        print ('-------------')
        print ('| ' + self.drawable_piece(self.state[1, 0]) + ' | ' + \
            self.drawable_piece(self.state[1, 1]) + ' | ' + \
            self.drawable_piece(self.state[1, 2]) + ' |')
        print ('-------------')
        print ('| ' + self.drawable_piece(self.state[2, 0]) + ' | ' + \
            self.drawable_piece(self.state[2, 1]) + ' | ' + \
            self.drawable_piece(self.state[2, 2]) + ' |')
        print ('-------------')

    def reset_state(self):
        """Reset the current board to be empty."""
        self.state = np.array([[Piece.Empty] * 3,
                              [Piece.Empty] * 3,
                              [Piece.Empty] * 3])
