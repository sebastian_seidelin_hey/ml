import os
import numpy as np
import tensorflow as tf
from tensorflow import keras
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from sklearn.preprocessing import MinMaxScaler
from tensorflow.keras.layers import Bidirectional, Dropout, Activation, Dense, LSTM
from tensorflow.python.keras.layers import CuDNNLSTM
from tensorflow.keras.models import Sequential

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.get_logger().setLevel('ERROR')

df = pd.read_csv('BTC-USD.csv', parse_dates=['Date'])
df = df.sort_values('Date')

ax = df.plot(x='Date', y='Close')
ax.set_xlabel('Date')
ax.set_ylabel('Close Price (USD)')
plt.show()

# Normalizing the data set
scaler = MinMaxScaler()
close_price = df.Close.values.reshape(-1, 1)
scaled_close_price = scaler.fit_transform(close_price)

scaled_close_price = scaled_close_price[~np.isnan(scaled_close_price)]
scaled_close_price = scaled_close_price.reshape(-1, 1)

# Preprocessing
SEQUENCE_LENGTH = 100

def to_sequence(data, sequence_length=SEQUENCE_LENGTH):
    d = []

    for index in range(len(data) - sequence_length):
        d.append(data[index: index + sequence_length])

    return np.array(d)

def pre_process(data_raw, sequence_length=SEQUENCE_LENGTH, train_split=0.1):
    data = to_sequence(data_raw, sequence_length=sequence_length)
    num_train = int(train_split * data.shape[0])

    X_train = data[:num_train, :-1,:]
    y_train = data[:num_train, -1,:]

    X_test = data[num_train:, :-1,:]
    y_test = data[num_train:, -1,:]

    return X_train, y_train, X_test, y_test

X_train, y_train, X_test, y_test = pre_process(scaled_close_price, SEQUENCE_LENGTH, train_split=0.95)

MODEL_DROPOUT = 0.2
WINDOW_SIZE = SEQUENCE_LENGTH - 1

model = keras.Sequential()
# First LSTM layer
model.add(Bidirectional(CuDNNLSTM(WINDOW_SIZE, return_sequences=True), input_shape=(WINDOW_SIZE, X_train.shape[-1])))
model.add(Dropout(rate=MODEL_DROPOUT))
# Second LSTM layer
model.add(Bidirectional(CuDNNLSTM((WINDOW_SIZE * 2), return_sequences=True)))
model.add(Dropout(rate=MODEL_DROPOUT))
# Third LSTM layer
model.add(Bidirectional(CuDNNLSTM(WINDOW_SIZE, return_sequences=False)))

model.add(Dense(units=1))

model.add(Activation('linear'))

# Training the model
model.compile(loss='mean_squared_error', optimizer='adam')
history = model.fit(X_train, y_train, epochs=50, batch_size=64, shuffle=False, validation_split=0.1)

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

y_hat = model.predict(X_test)

y_test_inverese = scaler.inverse_transform(y_test)
y_hat_inverse = scaler.inverse_transform(y_hat)

plt.plot(y_test_inverese, label='Actual Price', color='green')
plt.plot(y_hat_inverse, label='Predicted Price', color='red')
plt.title('Price Prediction')
plt.xlabel('Time [days]')
plt.ylabel('Price')
plt.legend(loc='best')
plt.show()
