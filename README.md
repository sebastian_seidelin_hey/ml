# ML Repository
This is a repository that contains code from playing around with machine learning. Currently projects are focused on deep and reinforcement learning from the datasets and environments distributed with [Keras](https://keras.io/) and [OpenAI GYM](https://gym.openai.com/). Check out [Boston Housing](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/boston_housing/) for a a simple example of using deep learning to predict house prices.

## Reinforcement Learning
* [Taxi](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/taxi/) (Q-learning)
* [Tic Tac Toe](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/tic_tac_toe/) (Q-learning)
* [Cart Pole](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/cartpole/) (Deep Q-learning)
* [Atari](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/atari/) (Deep Q-learning)

## Deep Learning
* [Boston Housing Dataset](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/boston_housing/) (NN)
* [MNIST](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/mnist/) (CNN, NN)
* [MNIST Fashion](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/fashion/) (CNN)
* [CIFAR 10](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/cifar10/) (CNN)
* [CIFAR 100](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/cifar100/) (CNN)
* [IMDB](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/imdb/) (LSTM)
* [Reuters](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/reuters/) (LSTM, NN)
* [Time Series Prediction](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/time_series/) (LSTM)

## Mixture
* [Mammographic Mass](https://bitbucket.org/sebastian_seidelin_hey/ml/src/master/mammographic_mass/) (Decision Tree, Random Forest Classifier, Support Vector Machine, KNN, Naive Bayes, Logistic Regression, Artificial Neural Network)

_Whatever you find here is provided as-is, feel free to use it at your own peril in whatever context you might find appropriate or applicable, I will not cover any losses you might have suffered through the usage of the software that is contained as part of this repository._