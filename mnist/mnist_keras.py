# To make TF shut up ...
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.get_logger().setLevel('ERROR')

from tensorflow import keras
from tensorflow.keras.datasets import mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import RMSprop

def evaluate_model(model, verbose=2, print_summary=True, print_score=True):
    print("Evaluating model ...")
    if print_summary:
        model.summary()
    model.compile(loss='categorical_crossentropy',
              optimizer=RMSprop(),
              metrics=['accuracy'])

    model.fit(train_images, train_labels,
                    batch_size=100,
                    epochs=10,
                    verbose=verbose,
                    validation_data=(test_images, test_labels))

    score = model.evaluate(test_images, test_labels, verbose=0)
    if print_score:
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
    
    return score

(mnist_train_images, mnist_train_labels), (mnist_test_images, mnist_test_labels) = mnist.load_data()

train_images = mnist_train_images.reshape(60000, 784)
test_images = mnist_test_images.reshape(10000, 784)
train_images = train_images.astype('float32')
test_images = test_images.astype('float32')
train_images /= 255
test_images /= 255

train_labels = keras.utils.to_categorical(mnist_train_labels, 10)
test_labels = keras.utils.to_categorical(mnist_test_labels, 10)

model = Sequential()
model.add(Dense(512, activation='relu', input_shape=(784,)))
model.add(Dense(10, activation='softmax'))

model_one = Sequential()
model_one.add(Dense(512, activation='relu', input_shape=(784,)))
model_one.add(Dropout(0.2))
model_one.add(Dense(512, activation='relu'))
model_one.add(Dropout(0.2))
model_one.add(Dense(10, activation='softmax'))

evaluate_model(model)
evaluate_model(model_one)