from keras.datasets import boston_housing
import os
import tensorflow as tf
from sklearn.preprocessing import MinMaxScaler,StandardScaler
from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import RMSprop
import numpy as np
import matplotlib.pyplot as plt
from tensorflow.keras.utils import plot_model
import pandas as pd
from pandas.plotting import table
from tabulate import tabulate

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
tf.get_logger().setLevel('ERROR')

(x_train, y_train), (x_test, y_test) = boston_housing.load_data()

features = np.concatenate((x_train,x_test))
targets = np.concatenate((y_train, y_test))

plt.hist(targets, bins=50) # One bin per k value (max is 50k)
plt.title('Targets Histogram')
plt.xlabel('Median Value (k$)')
plt.ylabel('Count')
plt.savefig('./figures/targets.svg')
plt.show()

headers = ['Feature:', '1', '2', '3', '4', '5', '6', '7', '8','9', '10', '11', '12', '13']
features_df = pd.DataFrame(features)
print(tabulate(features_df.describe(), tablefmt='github', headers=headers))

scaler = StandardScaler()
scaler.fit(features)
scaled_x_train = scaler.transform(x_train)
scaled_x_test = scaler.transform(x_test)

model = Sequential()
model.add(Dense(256, input_dim=13, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1, activation='linear'))
model.compile(loss='mean_squared_error', optimizer='adam')

history = model.fit(scaled_x_train, y_train,
          batch_size=64,
          epochs=50,
          verbose=0,
          validation_data=(scaled_x_test, y_test))

print(model.evaluate(scaled_x_test, y_test))

plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
plt.savefig('./figures/loss.svg')
plt.show()

y_hat = model.predict(scaled_x_test)

plt.plot(y_test, label='Actual Price', color='green')
plt.plot(y_hat, label='Predicted Price', color='red')
plt.title('Prediction')
plt.legend(loc='best')
plt.xlabel('Sample')
plt.ylabel('Median Value (k$)')
plt.savefig('./figures/prediction.svg')
plt.show()

#plot_model(model, to_file='./figures/model.png', show_shapes=True)