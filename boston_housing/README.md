# Abstract
In this small project I show that using a relatively simple neural network you can actually predict the house prices from the data set called Boston Housing prices reasonably well with very little effort.

# Data
The dataset is hosted by [Keras](https://keras.io/datasets/) and thus has little to no explanation of features other than the amount (which is 13) and that the target is the median house value in k$. So this is a regression problem. The only data preparation I have done is to scale the feauteres using a `StandardScaler()`. The data was already split into a test and train set from Keras so no need to do that. A summary of the features can be seen below, from the data it is clear that it has to be scaled before it can be used for the regression:

| Feature:   |          1 |        2 |         3 |          4 |          5 |          6 |        7 |         8 |         9 |      10 |        11 |       12 |        13 |
|------------|------------|----------|-----------|------------|------------|------------|----------|-----------|-----------|---------|-----------|----------|-----------|
| count      | 506        | 506      | 506       | 506        | 506        | 506        | 506      | 506       | 506       | 506     | 506       | 506      | 506       |
| mean       |   3.61352  |  11.3636 |  11.1368  |   0.06917  |   0.554695 |   6.28463  |  68.5749 |   3.79504 |   9.54941 | 408.237 |  18.4555  | 356.674  |  12.6531  |
| std        |   8.60155  |  23.3225 |   6.86035 |   0.253994 |   0.115878 |   0.702617 |  28.1489 |   2.10571 |   8.70726 | 168.537 |   2.16495 |  91.2949 |   7.14106 |
| min        |   0.00632  |   0      |   0.46    |   0        |   0.385    |   3.561    |   2.9    |   1.1296  |   1       | 187     |  12.6     |   0.32   |   1.73    |
| 25%        |   0.082045 |   0      |   5.19    |   0        |   0.449    |   5.8855   |  45.025  |   2.10018 |   4       | 279     |  17.4     | 375.377  |   6.95    |
| 50%        |   0.25651  |   0      |   9.69    |   0        |   0.538    |   6.2085   |  77.5    |   3.20745 |   5       | 330     |  19.05    | 391.44   |  11.36    |
| 75%        |   3.67708  |  12.5    |  18.1     |   0        |   0.624    |   6.6235   |  94.075  |   5.18843 |  24       | 666     |  20.2     | 396.225  |  16.955   |
| max        |  88.9762   | 100      |  27.74    |   1        |   0.871    |   8.78     | 100      |  12.1265  |  24       | 711     |  22       | 396.9    |  37.97    |

The following histogram shows the distribution of the targets:

![Targets Histogram Plot](./figures/targets.svg)

As such the data somewhat ressembles a normal distribution and is centralized around 20k$ and has a long tail towards 50k$ where we find a spike around 50k$. I have done nothing to preprocess the targets.

# Model
I have created a simple neural network with the following architecture based on dropout at each layer to prevent overfit:
```python
model = Sequential()
model.add(Dense(256, input_dim=13, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(256, activation='relu'))
model.add(Dropout(0.2))
model.add(Dense(1, activation='linear'))
model.compile(loss='mean_squared_error', optimizer='adam')
```

# Training & Prediction
Running above model on the data set yields the following loss per epoch of training:

![Loss plot](./figures/loss.svg "Loss")

The following diagram shows how well the prices are predicted by the model:

![Prediction plot](./figures/prediction.svg)

> In the diagram above the y-axis represents the house price and the x-axis represents the sample number (as it appeared in the test set).

# Discussion
The model convergers rather fast (<10 epochs) and from a visual inspection of the predictions these are not that far off.

# Conclusion
After scaling the features and creating a model based on a relatively simple neural network with three layers each containing 256 nodes and 0.2 dropoff at each layer it was possible to reasonible well predict the house prices in the Boston Housing set.

# Future Works
One could try to bring down the amount of features by doing principle component analysis. The hyper paramters of the model could also be tested (amount of layers, nodes in each layer, dropout, optimizer, etc.). Other models that are not based on deep learning should also be tried and evaluated, for instance, random forrest regression. Another thing to consider is to generate synthetic data to balance the data set such that the bell shape of the data will not impact the prediction as much.