import copy
import gym
import os
import sys
import random
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from gym import wrappers
from datetime import datetime
from scipy.misc import imresize

MAX_EXPERIENCES = 500000
MIN_EXPERIENCES = 50000
TARGET_UPDATE_PERIOD = 10000
IM_SIZE = 80

def downsample_image(A):
    B = A[31:195]
    B = B.mean(axis=2)
    B = imresize(B, size=(IM_SIZE, IM_SIZE), interp='nearest')
    return B

