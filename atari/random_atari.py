import gym
from statistics import mean 

env = gym.make('SpaceInvaders-ram-v0')
observation = env.reset()
total_reward = 0
rewards = []
for _ in range(2000):
    done = False
    while not done:
        #env.render()
        action = env.action_space.sample() # your agent here (this takes random actions)
        observation, reward, done, info = env.step(action)
        total_reward += reward

        if done:
            observation = env.reset()
            rewards.append(total_reward)
            total_reward = 0

env.close()
#print(rewards[:10])
print("Mean of rewards: ", mean(rewards), " Max: ", max(rewards), " Min: ", min(rewards))

# Yields: 149.9775