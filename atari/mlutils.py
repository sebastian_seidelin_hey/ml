import random
import numpy as np
from collections import deque
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Conv1D, Conv2D, Flatten, MaxPooling1D, GlobalAveragePooling1D
from tensorflow.keras.optimizers import Adam, RMSprop
import cv2

class DQNAgent:
    def __init__(self, observation_space, action_space, num_hidden_layers=1, hidden_layer_nodes=512, layer_dropout_rate=0.0, layer_activation='relu', exploration_rate=1.0, memory_size=1000000, learning_rate=0.0001, batch_size=32, min_learning=100000, gamma=0.99, state_buffer_size=4, optimizer='adam'):
        
        self.exploration_rate = exploration_rate
        self.action_space = action_space
        self.observation_space = observation_space
        self.memory = deque(maxlen=memory_size) # TODO: Find a more optimal way of keeping the frames
        self.batch_size = batch_size
        self.min_learning = min_learning
        self.gamma = gamma
        self.state_buffer_size = max(state_buffer_size,1)
        self.state_buffer = deque(maxlen=self.state_buffer_size)
        self.model = Sequential()

        # Setting up the convolutional layers
        self.model.add(Conv1D(filters=32, kernel_size=2, activation='relu', input_shape=(state_buffer_size,  observation_space)))
        self.model.add(Conv1D(filters=64, kernel_size=2, activation='relu'))
        self.model.add(Conv1D(filters=128, kernel_size=2, activation='relu'))
        #self.model.add(Conv1D(filters=32, kernel_size=1, activation='relu'))
        #self.model.add(MaxPooling1D(pool_size=1))
        #self.model.add(Conv1D(filters=16, kernel_size=1, activation='relu'))
        #self.model.add(Conv1D(filters=128, kernel_size=1, activation='relu'))
        self.model.add(GlobalAveragePooling1D())
        self.model.add(Dropout(0.2))
        self.model.add(Flatten())
        first_layer = False
        # Setting up the NN
        for _ in range(num_hidden_layers):

            if first_layer:
                self.model.add(Dense(hidden_layer_nodes, activation=layer_activation, input_shape=(state_buffer_size * observation_space,)))
                first_layer = False
            else:
                self.model.add(Dense(hidden_layer_nodes, activation=layer_activation))
            
            if layer_dropout_rate > 0.0:
                self.model.add(Dropout(layer_dropout_rate))
        #self.model.add(Flatten())
        self.model.add(Dense(self.action_space)) # Linear activation is inferred ...
        if optimizer is 'rms_prop':
            self.model.compile(loss="mean_squared_error", optimizer=RMSprop(lr=learning_rate, rho=0.95, epsilon=0.01), metrics=['accuracy'])
        else: # The 'adam' case
            self.model.compile(loss="mean_squared_error", optimizer=Adam(lr=learning_rate))

        #print(self.model.summary())

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state, eps):
        if np.random.rand() < eps or len(self.state_buffer) < self.state_buffer_size:
            return random.randrange(self.action_space)
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def update_state_buffer(self, state):
        state = np.array(state)
        #state = state / 255.0
        self.state_buffer.append(state)

    def get_full_state(self):
        if len(self.state_buffer) < self.state_buffer_size:
            return None
        
        #full_state = np.ndarray.flatten(np.array(self.state_buffer))
        full_state = np.array(self.state_buffer)
        
        return np.reshape(full_state, [1, self.state_buffer_size, self.observation_space])

    def reset_state_buffer(self):
        self.state_buffer.clear()
    
    def experience_replay(self, training_solver):
        if len(self.memory) < self.min_learning:
            return
        batch = random.sample(self.memory, self.batch_size)
        for state, action, reward, next_state, done in batch:
            q_update = reward
            if not done:
                q_update = (reward + self.gamma * np.amax(training_solver.model.predict(next_state)[0]))
            q_values = training_solver.model.predict(state)
            q_values[0][action] = q_update
            self.model.fit(state, q_values, verbose=0)

    def copy_from(self, other_solver):
        self.model.set_weights(other_solver.model.get_weights())

    def save_model(self, path="", name=""):
        assert len(path) > 0 and len(name) > 0, "Please specify both path and name"
        self.model.save(path+name+".h5")

    def print_model(self):
        print(self.model.summary())

class DQNAgentConv2D:
    def __init__(self, observation_space, action_space, num_hidden_layers=1, hidden_layer_nodes=512, layer_dropout_rate=0.0, layer_activation='relu', exploration_rate=1.0, memory_size=500000, learning_rate=0.0001, batch_size=32, min_learning=50000, gamma=0.99, state_buffer_size=4, optimizer='adam'):
        
        self.exploration_rate = exploration_rate
        self.action_space = action_space
        self.observation_space = observation_space
        self.memory = deque(maxlen=memory_size) # TODO: Find a more optimal way of keeping the frames
        self.batch_size = batch_size
        self.min_learning = min_learning
        self.gamma = gamma
        self.state_buffer_size = max(state_buffer_size,1)
        self.state_buffer = deque(maxlen=self.state_buffer_size)
        self.model = Sequential()
        input_shape = (IM_SIZE, IM_SIZE, 1) # 84, 84, 1
        self.model.add(Conv2D(32,
                              8,
                              strides=(4, 4),
                              padding="valid",
                              activation="relu",
                              input_shape=input_shape))
        self.model.add(Conv2D(64,
                              4,
                              strides=(2, 2),
                              padding="valid",
                              activation="relu",
                              input_shape=input_shape))
        self.model.add(Conv2D(64,
                              3,
                              strides=(1, 1),
                              padding="valid",
                              activation="relu",
                              input_shape=input_shape))
        self.model.add(Flatten())
        self.model.add(Dense(512, activation="relu"))
        self.model.add(Dense(action_space))
        self.model.compile(loss="mean_squared_error",
                           optimizer=RMSprop(lr=0.00025,
                                             rho=0.95,
                                             epsilon=0.01),
                           metrics=["accuracy"])

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state, eps):
        if np.random.rand() < eps or len(self.state_buffer) < self.state_buffer_size:
            return random.randrange(self.action_space)
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])

    def update_state_buffer(self, state):
        #new_state = downsample_image(state)
        #state = np.array(state)
        #state = state / 255.0
        self.state_buffer.append(process(state))

    def get_full_state(self):
        if len(self.state_buffer) < self.state_buffer_size:
            return None
        
        #full_state = np.ndarray.flatten(np.array(self.state_buffer))
        full_state = np.array(self.state_buffer)
        
        return full_state

    def reset_state_buffer(self):
        self.state_buffer.clear()
    
    def experience_replay(self, training_solver):
        if len(self.memory) < self.min_learning:
            return
        batch = random.sample(self.memory, self.batch_size)
        for state, action, reward, next_state, done in batch:
            q_update = reward
            if not done:
                q_update = (reward + self.gamma * np.amax(training_solver.model.predict(next_state)[0]))
            q_values = training_solver.model.predict(state)
            q_values[0][action] = q_update
            self.model.fit(state, q_values, verbose=0)

    def copy_from(self, other_solver):
        self.model.set_weights(other_solver.model.get_weights())

    def save_model(self, path="", name=""):
        assert len(path) > 0 and len(name) > 0, "Please specify both path and name"
        self.model.save(path+name+".h5")

    def print_model(self):
        print(self.model.summary())

IM_SIZE = 84

def process(frame):
    if frame.size == 210 * 160 * 3:
        img = np.reshape(frame, [210, 160, 3]).astype(np.float32)
    elif frame.size == 250 * 160 * 3:
        img = np.reshape(frame, [250, 160, 3]).astype(np.float32)
    else:
        assert False, "Unknown resolution."
    img = img[:, :, 0] * 0.299 + img[:, :, 1] * 0.587 + img[:, :, 2] * 0.114
    resized_screen = cv2.resize(img, (84, 110), interpolation=cv2.INTER_AREA)
    x_t = resized_screen[18:102, :]
    x_t = np.reshape(x_t, [84, 84, 1])
    return x_t.astype(np.uint8)