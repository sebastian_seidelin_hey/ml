import random
import gym
import numpy as np
from collections import deque
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
import os
import sys
from datetime import datetime
from gym import wrappers
from mlutils import DQNAgentConv2D

# To make TF shut up ...
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

CDefault      = '\033[39m'
CBlack        = '\033[30m'
CRed          = '\033[31m'
CGreen        = '\033[32m'
CYellow       = '\033[33m'
CBlue         = '\033[34m'
CMagenta      = '\033[35m'
CCyan         = '\033[36m'
CLightGray    = '\033[37m'
CDarkGray     = '\033[90m'
CLightRed     = '\033[91m'
CLightGreen   = '\033[92m'
CLightYellow  = '\033[93m'
CLightBlue    = '\033[94m'
CLightMagenta = '\033[95m'
CLightCyan    = '\033[96m'
CWhite        = '\033[97m'
CReset        = '\033[0m'

CBOLD     = '\33[1m'
CITALIC   = '\33[3m'
CURL      = '\33[4m'
CBLINK    = '\33[5m'
CBLINK2   = '\33[6m'
CSELECTED = '\33[7m'

CBLACKBG  = '\33[40m'
CREDBG    = '\33[41m'
CGREENBG  = '\33[42m'
CYELLOWBG = '\33[43m'
CBLUEBG   = '\33[44m'
CVIOLETBG = '\33[45m'
CBEIGEBG  = '\33[46m'
CWHITEBG  = '\33[47m'

CGREY    = '\33[90m'
CRED2    = '\33[91m'
CGREEN2  = '\33[92m'
CYELLOW2 = '\33[93m'
CBLUE2   = '\33[94m'
CVIOLET2 = '\33[95m'
CBEIGE2  = '\33[96m'
CWHITE2  = '\33[97m'

CGREYBG    = '\33[100m'
CREDBG2    = '\33[101m'
CGREENBG2  = '\33[102m'
CYELLOWBG2 = '\33[103m'
CBLUEBG2   = '\33[104m'
CVIOLETBG2 = '\33[105m'
CBEIGEBG2  = '\33[106m'
CWHITEBG2  = '\33[107m'

ENV_NAME = 'SpaceInvaders-v0'

VIDEO_PERIOD = 100 # For when 'monitor' is given as argument

# Params to control explore-exploit logic
EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.1
EXPLORATION_DECAY = 0.9995

# Parameters used to control the overall execution and structure of the NN
COPY_PERIOD = 1000
NUM_EPISODES = 3500
NUM_HIDDEN_LAYERS = 1
NUM_HIDDEN_NODES = 256
DROPOUT_RATE = 0.2
SHOULD_PREFILL_MEMORY = True

def game_loop():
    env = gym.make(ENV_NAME)
    # Score logger

    # This will generate periodical video clips, currently every 100th episode
    if 'monitor' in sys.argv:
        filename = os.path.basename(__file__).split('.')[0]
        file_path = os.path.dirname(os.path.abspath(__file__))
        monitor_dir = os.path.join(file_path, filename + '_' + str(datetime.now()).replace('.','_').replace(' ','').replace(':','_') + '_vidoes')
        env = wrappers.Monitor(env, monitor_dir, video_callable=lambda episode_id: episode_id%VIDEO_PERIOD==0)

    observation_space = env.observation_space.shape
    action_space = env.action_space.n
    dqn_solver = DQNAgentConv2D(observation_space, action_space, optimizer='adam', num_hidden_layers=NUM_HIDDEN_LAYERS, hidden_layer_nodes=NUM_HIDDEN_NODES, layer_dropout_rate=DROPOUT_RATE)
    dqn_solver_train = DQNAgentConv2D(observation_space, action_space, optimizer='adam', num_hidden_layers=NUM_HIDDEN_LAYERS, hidden_layer_nodes=NUM_HIDDEN_NODES, layer_dropout_rate=DROPOUT_RATE)
    
    dqn_solver.print_model()
    
    N = NUM_EPISODES
    copy_period = COPY_PERIOD
    total_rewards = []
    means_over_last_hundred_episodes = []
    eps = 1.0

    if SHOULD_PREFILL_MEMORY:
        print(colorize_string("Pre-Filling the Replay Memory", CLightMagenta))
        while len(dqn_solver.memory) < dqn_solver.min_learning:
            dqn_solver.reset_state_buffer()
            state = env.reset()
            dqn_solver.update_state_buffer(state)
            total_reward = 0
            terminal = False
            while not terminal:
                full_state = dqn_solver.get_full_state()
                action = dqn_solver.act(full_state, eps)
                state_next, reward, terminal, _ = env.step(action)
                total_reward += reward
                reward = reward if not terminal else -100

                dqn_solver.update_state_buffer(state_next)
                next_full_state = dqn_solver.get_full_state()

                if full_state is not None and next_full_state is not None:
                    dqn_solver.remember(full_state, action, reward, next_full_state, terminal)

                state = state_next
                # Note that we are not doing experience replay here ...
            print('\r',"{0} steps added to memory".format(len(dqn_solver.memory)), sep='', end='', flush=True)
    else:
        print(colorize_string("Skipping memory pre-fill", CLightMagenta))

    print(" -",colorize_string("Done, memory ready", CLightGreen)) # All good, now the fun begins ...
    print(colorize_string("Started Playing ...", CLightMagenta))
    for n in range(N):
        dqn_solver.reset_state_buffer()
        state = env.reset()
        #state = np.reshape(state, [1, observation_space])
        dqn_solver.update_state_buffer(state)
        step = 0
        total_reward = 0
        terminal = False
        eps = max(EXPLORATION_MIN, eps * EXPLORATION_DECAY)
        #eps = 0.2
        #eps = max(EXPLORATION_MIN, 1.0 / np.sqrt(n+1))
        #eps = max(EXPLORATION_MIN, 1.0 / (n+1))
        copy = 0
        start_time = datetime.now()
        while not terminal:
            step += 1
            print('\r',"Run: ", colorize_string("{0}".format(n), CYellow), " Epsilon: ", colorize_string("{0:.3f}".format(eps), CBlue), " Step: ", colorize_string("{0}".format(step), CCyan), " Score: ", colorize_string("{0}".format(total_reward), CGreen), " ", copy * colorize_string("C", CMagenta), " ", sep='', end='', flush=True)
            full_state = dqn_solver.get_full_state()
            action = dqn_solver.act(full_state, eps)
            state_next, reward, terminal, _ = env.step(action)
            total_reward += reward
            #reward = -1 if reward is 0 else reward
            #reward = reward if not terminal else -100 
            
            dqn_solver.update_state_buffer(state_next)
            next_full_state = dqn_solver.get_full_state()
            
            if full_state is not None and next_full_state is not None:
                dqn_solver.remember(full_state, action, reward, next_full_state, terminal)
            
            state = state_next
            dqn_solver.experience_replay(dqn_solver_train)
            if step % copy_period == 0:
                if len(dqn_solver.memory) > dqn_solver.min_learning:
                    dqn_solver_train.copy_from(dqn_solver)
                    copy += 1
                #print(colorize_string("Copying", CMagenta) + " network w. current score: {0}".format(total_reward))
            if terminal:
                end_time = datetime.now() - start_time
                
                #print(colorize_string("Ended", CRed) + " after {0} steps".format(step))
                total_rewards.append(total_reward)
                if len(total_rewards) >= 100:
                    mean_over_last_hundred_episodes = np.mean(total_rewards[-100:])
                    means_over_last_hundred_episodes.append(mean_over_last_hundred_episodes)
                    print("Avg./100: {0:.1f} ".format(mean_over_last_hundred_episodes), end='')
                    #if mean_over_last_hundred_episodes > 195.0:
                        #print("Summary: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue) + ", Avg./100: " + colorize_string(str(mean_over_last_hundred_episodes), CGreen))
                    #else:
                        #print("Summary: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue) + ", Avg./100: " + colorize_string(str(mean_over_last_hundred_episodes), CRed))
                #else:
                    #print("Summary: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue))
                
                print("Duration:", colorize_string("{0}s".format(end_time.seconds), CLightYellow))
                break
        
    print("Done playing, avg reward for last 100 episodes:", np.mean(total_rewards[-100:]))
    for i in range(0, len(means_over_last_hundred_episodes)):
        if means_over_last_hundred_episodes[i] > 195:
            print("Converged after epsiode: " + colorize_string(str(i+100), CGreen))
            break

    if 'plot' in sys.argv:
        filename = os.path.basename(__file__).split('.')[0]
        file_path = os.path.dirname(os.path.abspath(__file__))
        model_dir = os.path.join(file_path, filename + '_' + str(datetime.now()).replace('.','_').replace(' ','').replace(':','_') + '_plots')
        if not os.path.exists(model_dir):
            os.makedirs(model_dir)
        
        print("Saving plots to: " + colorize_string(model_dir, CLightBlue))

        rewards_plot = plt.figure()
        plt.plot(total_rewards)
        plt.title("Rewards")
        rewards_plot.savefig(model_dir + '/rewards.pdf')

        means_plot = plt.figure()
        plt.plot(means_over_last_hundred_episodes)
        plt.title("Means over last 100 episodes")
        means_plot.savefig(model_dir + '/means.pdf')

    if 'save' in sys.argv:
        filename = os.path.basename(__file__).split('.')[0]
        file_path = os.path.dirname(os.path.abspath(__file__))
        model_dir = os.path.join(file_path, filename + '_' + str(datetime.now()).replace('.','_').replace(' ','').replace(':','_') + '_models')
        if not os.path.exists(model_dir):
            os.makedirs(model_dir)

        dqn_solver.save_model(model_dir, "solver_model")
        dqn_solver_train.save_model(model_dir, "train_model")
        

def main():
    game_loop()

def colorize_string(message, color=CWhite):
    return color + message + CReset

if __name__ == '__main__':
    main()