# To make TF shut up ...
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.get_logger().setLevel('ERROR')

import pandas as pd
import numpy as np


names=['BI-RADS','Age','Shape','Margin','Density','Severity']
df = pd.read_csv("mammographic_masses.data.txt", names=names, na_values=['?'])

df = df.dropna()

feature_names = ['BI-RADS','Age','Shape','Margin','Density']

features = df[feature_names].values
labels = df['Severity'].values

from sklearn import preprocessing

scaler = preprocessing.StandardScaler()
features_scaled = scaler.fit_transform(features)

import numpy as np
from sklearn.model_selection import train_test_split

X_train, X_test, y_train, y_test = train_test_split(features_scaled, labels, test_size=0.25, random_state=42)

from sklearn import tree
lr_clf = tree.DecisionTreeClassifier()
lr_clf = lr_clf.fit(X_train,y_train)
#print("Decision Tree Classifier score: {0}".format(clf.score(X_test, y_test)))

from sklearn.model_selection import cross_val_score

cv_scores = cross_val_score(lr_clf, X_test, y_test, cv=10)
#print("Score: ",cv_scores)
print("Decision Tree Classifier score mean:",cv_scores.mean())

from sklearn.ensemble import RandomForestClassifier

forrest_clf = RandomForestClassifier(n_estimators=2)
forrest_clf = lr_clf.fit(X_train, y_train)
forrest_cv_scores = cross_val_score(forrest_clf, X_test, y_test, cv=10)
#print("Score: ",forrest_cv_scores)
print("Random Forrest Classifier score mean:",forrest_cv_scores.mean())

from sklearn import svm
svm_clf = svm.SVC(kernel='linear', C=1, gamma='scale').fit(X_train, y_train)
svm_clf.score(X_test, y_test)
svm_cv_scores = cross_val_score(svm_clf, X_test, y_test, cv=10)
print("SVM Linear mean of scores:",svm_cv_scores.mean())

svm_clf_rbf = svm.SVC(kernel='rbf', gamma='scale').fit(X_train, y_train)
svm_clf_rbf.score(X_test, y_test)
svm_clf_rbf_scores = cross_val_score(svm_clf_rbf, X_test, y_test, cv=10)
print("SVM RBF Kernel mean of scores:",svm_clf_rbf_scores.mean())

svm_clf_sig = svm.SVC(kernel='sigmoid', gamma='scale').fit(X_train, y_train)
svm_clf_sig.score(X_test, y_test)
svm_clf_sig_scores = cross_val_score(svm_clf_sig, X_test, y_test, cv=10)
print("SVM Sigmoid Kernel mean of scores:",svm_clf_sig_scores.mean())

svm_clf_poly = svm.SVC(kernel='poly', degree=3, gamma='scale').fit(X_train, y_train)
svm_clf_poly.score(X_test, y_test)
svm_clf_poly_scores = cross_val_score(svm_clf_poly, X_test, y_test, cv=10)
print("SVM Poly (degree 3) Kernel mean of scores:",svm_clf_poly_scores.mean())

from sklearn.neighbors import KNeighborsClassifier
neigh = KNeighborsClassifier(n_neighbors=10)
neigh.fit(X_train, y_train)
print("KNN w. n=10:", neigh.score(X_test, y_test))

scores = []
for n in range(50):
    neigh = KNeighborsClassifier(n_neighbors=n+1)
    neigh.fit(X_train, y_train)
    scores.append(neigh.score(X_test, y_test))

print("KNN n=1..50: max:", max(scores), "min:", min(scores))
#print("Scores: ", scores)

from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import MinMaxScaler

scaler = MinMaxScaler()
scaler.fit(X_train)

X_train_scaled = scaler.transform(X_train)

classifier = MultinomialNB()
classifier.fit(X_train_scaled, y_train)
print("Naive Bayes score:", classifier.score(scaler.transform(X_test), y_test))

from sklearn.linear_model import LogisticRegressionCV
lr_clf = LogisticRegressionCV(cv=10, random_state=0, multi_class='multinomial').fit(X_train, y_train)
print("Logistical Regression score: {0}".format(lr_clf.score(X_test, y_test)))

from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout
from tensorflow.keras.optimizers import RMSprop
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from sklearn.model_selection import cross_val_score

def create_model():
    model = Sequential()
    model.add(Dense(256, input_dim=5, activation='relu'))
    model.add(Dense(128, activation='relu'))
    model.add(Dense(64, activation='relu'))
    model.add(Dropout(0.5))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    # model.add(Dense(256, activation='relu'))
    # model.add(Dropout(0.5))
    model.add(Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    return model

estimator = KerasClassifier(build_fn=create_model, epochs=500, verbose=0)
    
cv_scores = cross_val_score(estimator, features, labels, cv=10)
print("NN Score: ", cv_scores.mean())