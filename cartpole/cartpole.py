import random
import gym
import numpy as np
from collections import deque
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.optimizers import Adam
import os
import sys
from datetime import datetime
from gym import wrappers

CDefault      = '\033[39m'
CBlack        = '\033[30m'
CRed          = '\033[31m'
CGreen        = '\033[32m'
CYellow       = '\033[33m'
CBlue         = '\033[34m'
CMagenta      = '\033[35m'
CCyan         = '\033[36m'
CLightGray    = '\033[37m'
CDarkGray     = '\033[90m'
CLightRed     = '\033[91m'
CLightGreen   = '\033[92m'
CLightYellow  = '\033[93m'
CLightBlue    = '\033[94m'
CLightMagenta = '\033[95m'
CLightCyan    = '\033[96m'
CWhite        = '\033[97m'
CReset        = '\033[0m'

ENV_NAME = 'CartPole-v1'

GAMMA = 0.99
LEARNING_RATE = 0.001

MEMORY_SIZE = 1000000
BATCH_SIZE = 40
MIN_LEARNING = 100

EXPLORATION_MAX = 1.0
EXPLORATION_MIN = 0.01
EXPLORATION_DECAY = 0.995

COPY_PERIOD = 10
NUM_EPISODES = 200

class DQNSolver:
    def __init__(self, observation_space, action_space):
        self.exploration_rate = EXPLORATION_MAX

        self.action_space = action_space
        self.memory = deque(maxlen=MEMORY_SIZE)

        self.model = Sequential()
        self.model.add(Dense(64, input_shape=(observation_space,), activation="relu"))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(64, activation="relu"))
        self.model.add(Dense(self.action_space, activation="linear"))
        self.model.compile(loss="mse", optimizer=Adam(lr=LEARNING_RATE))

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state, eps):
        if np.random.rand() < eps:
            return random.randrange(self.action_space)
        q_values = self.model.predict(state)
        return np.argmax(q_values[0])
    
    def experience_replay(self, training_solver):
        if len(self.memory) < MIN_LEARNING:
            return
        batch = random.sample(self.memory, BATCH_SIZE)
        for state, action, reward, next_state, done in batch:
            q_update = reward
            if not done:
                q_update = (reward + GAMMA * np.amax(training_solver.model.predict(next_state)[0]))
            q_values = training_solver.model.predict(state)
            q_values[0][action] = q_update
            self.model.fit(state, q_values, verbose=0)

    def copy_from(self, other_solver):
        self.model.set_weights(other_solver.model.get_weights())

def cartpole():
    env = gym.make(ENV_NAME)
    # Score logger

    if 'monitor' in sys.argv:
        filename = os.path.basename(__file__).split('.')[0]
        file_path = os.path.dirname(os.path.abspath(__file__))
        monitor_dir = os.path.join(file_path, filename + '_' + str(datetime.now()).replace('.','_').replace(' ','').replace(':','_') + '_vidoes')
        env = wrappers.Monitor(env, monitor_dir, video_callable=lambda episode_id: episode_id%20==0)

    observation_space = env.observation_space.shape[0]
    action_space = env.action_space.n
    dqn_solver = DQNSolver(observation_space, action_space)
    dqn_solver_train = DQNSolver(observation_space, action_space)
    N = NUM_EPISODES
    copy_period = COPY_PERIOD
    total_rewards = []
    means_over_last_hundred_episodes = []
    for n in range(N):
        state = env.reset()
        state = np.reshape(state, [1, observation_space])
        step = 0
        total_reward = 0
        terminal = False
        while not terminal:
            step += 1
            eps = max(EXPLORATION_MIN, 1.0 / np.sqrt(n+1))
            #eps = max(EXPLORATION_MIN, 1.0 / (n+1))
            action = dqn_solver.act(state, eps)
            state_next, reward, terminal, _ = env.step(action)
            total_reward += reward
            reward = reward if not terminal else -200
            state_next = np.reshape(state_next, [1, observation_space])
            dqn_solver.remember(state, action, reward, state_next, terminal)
            state = state_next
            dqn_solver.experience_replay(dqn_solver_train)
            if step % copy_period == 0:
                dqn_solver_train.copy_from(dqn_solver)
            if terminal:
                total_rewards.append(total_reward)
                if len(total_rewards) >= 100:
                    mean_over_last_hundred_episodes = np.mean(total_rewards[-100:])
                    means_over_last_hundred_episodes.append(mean_over_last_hundred_episodes)
                    if mean_over_last_hundred_episodes > 195.0:
                        print("Run: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue) + ", Avg./100: " + colorize_string(str(mean_over_last_hundred_episodes), CGreen))
                    else:
                        print("Run: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue) + ", Avg./100: " + colorize_string(str(mean_over_last_hundred_episodes), CRed))
                else:
                    print("Run: " + colorize_string(str(n), CYellow) + ", Exploration: " + colorize_string("{0:.3f}".format(eps), CCyan) + ", Score: " + colorize_string(str(total_reward), CBlue))
                break
        
    print("Done playing, avg reward for last 100 episodes:", np.mean(total_rewards[-100:]))
    for i in range(0, len(means_over_last_hundred_episodes)):
        if means_over_last_hundred_episodes[i] > 195:
            print("Converged after epsiode: " + colorize_string(str(i+100), CGreen))
            break

    if 'plot' in sys.argv:
        filename = os.path.basename(__file__).split('.')[0]
        file_path = os.path.dirname(os.path.abspath(__file__))
        plot_dir = os.path.join(file_path, filename + '_' + str(datetime.now()).replace('.','_').replace(' ','').replace(':','_') + '_plots')
        if not os.path.exists(plot_dir):
            os.makedirs(plot_dir)
        
        print("Saving plots to: " + colorize_string(plot_dir, CLightBlue))

        rewards_plot = plt.figure()
        plt.plot(total_rewards)
        plt.title("Rewards")
        rewards_plot.savefig(plot_dir + '/rewards.pdf')

        means_plot = plt.figure()
        plt.plot(means_over_last_hundred_episodes)
        plt.title("Means over last 100 episodes")
        means_plot.savefig(plot_dir + '/means.pdf')
        

def main():
    cartpole()

def colorize_string(message, color=CWhite):
    return color + message + CReset

if __name__ == '__main__':
    main()