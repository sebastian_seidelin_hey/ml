# To make TF shut up ...
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf
tf.get_logger().setLevel('ERROR')

import tensorflow
from tensorflow.keras.preprocessing import sequence
from tensorflow.keras.preprocessing.text import Tokenizer
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, BatchNormalization
from tensorflow.keras.datasets import reuters

num_words = 20000
num_categories = 46

(x_train, y_train), (x_test, y_test) = reuters.load_data(num_words=num_words)

tokenizer = Tokenizer(num_words=num_words)
x_train = tokenizer.sequences_to_matrix(x_train, mode='binary')
x_test = tokenizer.sequences_to_matrix(x_test, mode='binary')

train_labels = tensorflow.keras.utils.to_categorical(y_train, num_categories)
test_labels = tensorflow.keras.utils.to_categorical(y_test, num_categories)

model = Sequential()
model.add(Dense(512, activation='relu', input_shape=(num_words,)))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(512, activation='relu'))
model.add(BatchNormalization())
model.add(Dropout(0.5))
model.add(Dense(num_categories, activation='softmax'))

model.compile(loss='categorical_crossentropy',
              optimizer='adam',
              metrics=['accuracy'])

model.fit(x_train, train_labels,
          batch_size=64,
          epochs=30,
          verbose=2,
          validation_data=(x_test, test_labels))

score, acc = model.evaluate(x_test, test_labels,
                            batch_size=32,
                            verbose=2)
print('Test score:', score)
print('Test accuracy:', acc)